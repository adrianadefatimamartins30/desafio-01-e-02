*** Settings ***
Documentation     Cadastro de agendamento de apresentação
Library           SeleniumLibrary

*** Variables ***
${URL}            http://www.lekto.com.br

*** Test Cases ***
Cadastro de Agendamento de Apresentação
    Abrir Página de Agendamento
    Preencher Campos de Cadastro
    Submeter Formulário
    Verificar Sucesso do Cadastro

*** Keywords ***
*** Abrir Página de Agendamento *** 
    Open Browser    ${URL}    chrome

*** Preencher Campos de Cadastro *** 
    Input Text    id=nome    ${GENERATE_RANDOM_STRING}
    Input Text    id=email    ${GENERATE_RANDOM_EMAIL}
    Input Text    id=telefone    ${GENERATE_RANDOM_PHONE}
    Input Text    id=empresa    ${GENERATE_RANDOM_STRING}

*** Submeter Formulário *** 
    Click Button    id=submit

*** Verificar Sucesso do Cadastro *** 
    Wait Until Page Contains    Cadastro realizado com sucesso